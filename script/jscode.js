let enterPass = document.getElementById('enterPass');
let confirmPass = document.getElementById('confirmPass');

let seeIcon = document.getElementById('see');
let noSeeIcon = document.getElementById('dontSee');

let butt = document.getElementById('butt');

function appearIcon(arg1, arg2) {
  if (arg1.className === 'fas fa-eye icon-password') {
    arg1.className = 'fas fa-eye-slash icon-password';
    arg2.type = 'text'
  } else {
    arg1.className = 'fas fa-eye icon-password';
    arg2.type = 'password'
  }
}

butt.onclick = () => {
  let err = document.getElementById('error');
  if (enterPass.value === confirmPass.value) {
    err.type = 'text';
    err.innerText = 'Congratulate, you write correct pass !!!'
    err.style.color = 'green';
  } else {
    err.type = 'text';
    err.innerText = 'write correct pass pls';
    err.style.color = 'red';
  }
};

confirmPass.onfocus = () => {
  if (confirmPass.style.color === 'red') {
    confirmPass.value = '';
  }
  confirmPass.style.color = '';
}